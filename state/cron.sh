#!/usr/bin/env sh

THIS_DIR="$(cd "$(dirname "$0")" || exit 1; pwd)"

LOCKFILE=${LOCKFILE-"$THIS_DIR/cron.lock"}

if [ -f "$LOCKFILE" ]; then
    echo "locked:" "[$LOCKFILE]"
    exit 0
fi

{ date +%s; date; } > "$LOCKFILE"

cleanup() { [ -f "$LOCKFILE" ] && rm "$LOCKFILE"; }
trap cleanup EXIT

"$THIS_DIR/venv/bin/python3" "$THIS_DIR/run.py" "$@"

exit 0
