from collections import namedtuple
from json import dump, load
from json.decoder import JSONDecodeError
from logging import getLogger
from os import path

from requests import get as r_get

LOG = getLogger(__name__)
TIMEOUT = 5


class Glab:
    GL_PIPE = namedtuple('Pipe', ('status', 'proj', 'user'))
    GL_PROJ = namedtuple('Proj', ('id', 'name'))
    GL_USER = namedtuple('User', ('id', 'name'))

    def __init__(self, args):
        self._log = getLogger(self.__class__.__name__)
        self.args = args
        self.data = None

    @staticmethod
    def _defaults():
        return {
            'projects': {},
            'users': {},
        }

    def _load(self):
        if path.exists(self.args.store):
            with open(self.args.store, 'r') as handle:
                self._log.debug('loading data from [%s]', self.args.store)
                try:
                    return load(handle)
                except JSONDecodeError as ex:
                    self._log.exception(ex)
                    return None

        self._log.info('[%s] does not exist - using default', self.args.store)
        return self._defaults()

    def save(self):
        if not self.data:
            self._log.error('data is empty - refusing to store')
            return False

        with open(self.args.store, 'w') as handle:
            self._log.debug('dumping data to [%s]', self.args.store)
            if dump(self.data, handle, indent=2, sort_keys=True):
                return True
        return False

    def load(self):
        if self.data is None:
            self.data = self._load()
            if self.data is None:
                self._log.error('something is very wrong there...')
                return False
        return True

    @staticmethod
    def _dict_kv(find, **data):
        for key, val in data.items():
            if val == find:
                return key
        return None

    def _user_by_name(self, user_name):
        user_id = self.data['users'].get(user_name, None)
        if user_id is None:
            user_id = _find_user_id(self.args, user_name)
            if user_id is not None:
                self.data['users'][user_name] = user_id
                self.save()

        return self.GL_USER(id=user_id, name=user_name)

    def _project_by_id(self, proj_id):
        proj_name = self._dict_kv(proj_id, **self.data['projects'])
        if proj_name is None:
            proj_name = _find_project_name(self.args, proj_id)
            if proj_name is not None:
                self.data['projects'][proj_name] = proj_id
                self.save()

        return self.GL_PROJ(id=proj_id, name=proj_name)

    def update_pipeline(self, pipe):
        status = None
        if pipe and pipe.proj and pipe.user:
            self._log.info(
                'fetching pipeline status from [%s] for [%s]',
                pipe.proj.name, pipe.user.name
            )

            status = _fetch_recent_pipeline_state(
                self.args, pipe.proj.id, pipe.user.name
            )

        return self.GL_PIPE(status=status, proj=pipe.proj, user=pipe.user)

    def get_pipeline(self, user_name):
        proj = None
        user = self._user_by_name(user_name)
        if user is not None:
            proj = self._project_by_id(
                _fetch_recent_user_project_id(self.args, user.id)
            )

        return self.update_pipeline(
            self.GL_PIPE(status=None, proj=proj, user=user)
        )


def _find_user_id(args, user_name):
    LOG.debug('looking for user id by name [%s]', user_name)
    status = 'empty parameter'
    if user_name is not None:
        req = r_get(
            '/'.join((
                args.glurl, 'api', 'v4', 'users',
            )),
            headers={'PRIVATE-TOKEN': args.token},
            timeout=TIMEOUT,
            params={
                'username': user_name,
            },
        )
        status = req.status_code
        if status == 200:
            for elem in req.json():
                return elem.get('id', None)
            LOG.info(
                'search for user id by name [%s] got no results',
                user_name
            )
            return None

    LOG.warning(
        'search for user id by name [%s] got error [%s]',
        user_name, status
    )
    return None


def _find_project_name(args, proj_id):
    LOG.debug('looking for project name by id [%s]', proj_id)
    status = 'empty parameter'
    if proj_id is not None:
        req = r_get(
            '/'.join((
                args.glurl, 'api', 'v4', 'projects', f'{proj_id}',
            )),
            headers={'PRIVATE-TOKEN': args.token},
            timeout=TIMEOUT,
            params={},
        )
        status = req.status_code
        if status == 200:
            elem = req.json()
            if elem:
                return elem.get('path_with_namespace', None)
            LOG.info(
                'search for user id by name [%s] got no results',
                proj_id
            )
            return None

    LOG.warning(
        'search for project name by id [%s] got error [%s]',
        proj_id, status
    )
    return None


def _fetch_recent_user_project_id(args, user_id):
    LOG.debug('fetching recent project id by user id [%s]', user_id)
    status = 'empty parameter'
    if user_id is not None:
        req = r_get(
            '/'.join((
                args.glurl, 'api', 'v4', 'users', f'{user_id}', 'events',
            )),
            headers={'PRIVATE-TOKEN': args.token},
            timeout=TIMEOUT,
            params={
                'action': 'pushed',
            },
        )
        status = req.status_code
        if status == 200:
            for elem in req.json():
                return elem.get('project_id', None)
            LOG.info(
                'fetch for recent project id by user id [%s] got no results',
                user_id
            )
            return None

    LOG.warning(
        'fetch for recent project id by user id [%s] got error [%s]',
        user_id, status
    )
    return None


def _fetch_recent_pipeline_state(args, proj_id, user_name):
    LOG.debug('fetching recent state by project id [%s]', proj_id)
    status = 'empty parameter'
    if proj_id is not None and user_name is not None:
        req = r_get(
            '/'.join((
                args.glurl, 'api', 'v4', 'projects', f'{proj_id}', 'pipelines'
            )),
            headers={'PRIVATE-TOKEN': args.token},
            timeout=TIMEOUT,
            params={
                'username': user_name,
                'order_by': 'id',
                'sort': 'desc',
            },
        )
        status = req.status_code
        if status == 200:
            for elem in req.json():
                return elem.get('status', None)
            LOG.info(
                'fetch for recent state by project id [%s] got no results',
                proj_id
            )
            return None

    LOG.warning(
        'fetch for recent state by project id [%s] got [%s]',
        proj_id, status
    )
    return None
