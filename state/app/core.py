from logging import getLogger
from time import sleep

from app.glab import Glab
from app.lamp import Lamp
from app.yell import Yell


class Core:
    def __init__(self, args):
        self._log = getLogger(self.__class__.__name__)
        self.args = args
        self.glab = Glab(self.args)
        self.lamp = Lamp(self.args)
        self.yell = Yell(self.args)

    def pause(self, long=False):
        delay = self.args.delay
        if long:
            delay *= 2
        sleep(delay / 1000)

    def running(self, pipe):
        if pipe is not None and pipe.status is not None:
            if pipe.status == 'running':
                if self.args.blink:
                    self.lamp.wait()
                else:
                    self.lamp.note()
                self.pause()

                return pipe

            if self.args.blink:
                self.lamp.null()
                self.pause()
                self.lamp.full()
                self.pause()

            if pipe.status == 'failed':
                self.lamp.fail()
            elif pipe.status == 'success':
                self.lamp.good()
            self.pause(True)

        return pipe

    def monitor(self, pipes):
        for pipe in pipes:
            if pipe is not None and pipe.status is not None:
                if pipe.status == 'running':
                    self.running(pipe)
                    yield self.glab.update_pipeline(pipe)
                elif pipe.status == 'success':
                    self.yell.good.add(pipe.proj.name)
                elif pipe.status == 'failed':
                    self.yell.fail.add(pipe.proj.name)

    def __call__(self):
        if not self.glab.load():
            return False

        pipes = [
            self.glab.get_pipeline(user_name) for user_name in self.args.users
        ]
        while pipes:
            pipes = list(self.monitor(pipes))

        if self.yell.fail:
            self.lamp.fail()
        else:
            self.lamp.good()

        self._log.info(
            'good:\n[%s]\n\nfailed:\n[%s]',
            '] ['.join(sorted(self.yell.good)),
            '] ['.join(sorted(self.yell.fail)),
        )

        if self.args.shout:
            self.yell()

        self._log.info('all done')
        return True
