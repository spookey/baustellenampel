from app.args import arguments
from app.core import Core
from app.logs import log_setup


def run():
    args = arguments()
    log_setup(args.level)
    return 0 if Core(args)() else 1
