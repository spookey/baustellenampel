from argparse import ArgumentParser

from app.logs import LOG_LEVELS


def arguments():
    def _help(txt):
        return f'{txt} (default: [%(default)s])'

    parser = ArgumentParser(
        'state', add_help=True, epilog='c[_]',
    )

    parser.add_argument(
        '-s', '--store',
        default='store.json',
        help=_help('storage filename'),
    )
    parser.add_argument(
        '-l', '--level',
        choices=LOG_LEVELS, default='info',
        help=_help('log level'),
    )

    parser.add_argument(
        '-p', '--probe',
        default='http://probe.local',
        help='probe url',
    )
    parser.add_argument(
        '--blink',
        action='store_true',
        help=_help('blink lamp while building instead of static light'),
    )
    parser.add_argument(
        '-d', '--delay',
        type=int, default=500,
        help=_help('delay in ms when build is running'),
    )

    parser.add_argument(
        '--shout',
        type=int, default=0,
        help=_help('onscreen overview for ms afterwards'),
    )

    parser.add_argument(
        '-g', '--glurl',
        default='https://gitlab.org',
        help='gitlab base url',
    )
    parser.add_argument(
        '-t', '--token',
        required=True,
        help='api token',
    )
    parser.add_argument(
        'users',
        nargs='+', action='append',
        help='user names',
    )

    args = parser.parse_args()

    if not args.probe.startswith('http'):
        parser.exit('probe url must start with [http]')
    if not args.glurl.startswith('http'):
        parser.exit('gitlab base url must start with [http]')

    args.probe = args.probe.rstrip('/')
    args.glurl = args.glurl.rstrip('/')

    if args.delay < 500:
        parser.exit(f'delay [{args.delay}] is way too short!')

    args.users = [fl for at in args.users for fl in at]
    for user in args.users:
        if not user:
            parser.exit('empty user specified!')

    return args
