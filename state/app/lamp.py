from logging import getLogger

from requests import post as r_post
from requests.exceptions import RequestException

LOG = getLogger(__name__)
TIMEOUT = 10


class Lamp:
    def __init__(self, args):
        self._log = getLogger(self.__class__.__name__)
        self.args = args
        self.state = {
            'g': None,
            'r': None,
            'y': None,
        }

    def _needed(self, color, action):
        current = self.state.get(color, None)
        if action == 'toggle':
            self.state[color] = None
            return True
        if current is None or current != action:
            self.state[color] = action
            return True
        return False

    def _set(self, color, action):
        if not self._needed(color, action):
            self._log.debug('set lamp [%s] [%s] not needed', color, action)
            return True

        self._log.debug('set lamp [%s] [%s]', color, action)
        try:
            req = r_post(
                '/'.join((self.args.probe, 'light', color, action)),
                timeout=TIMEOUT,
            )
        except RequestException as ex:
            self._log.exception(ex)
        else:
            return req.status_code == 200

        self._log.warning('set lamp [%s] [%s] failed', color, action)
        return False

    def _lamp(self, rr_act, yy_act, gg_act):
        self._log.info('lamp [(r)%s] [(y)%s] [(g)%s]', rr_act, yy_act, gg_act)
        return all((
            self._set('r', rr_act),
            self._set('y', yy_act),
            self._set('g', gg_act),
        ))

    def null(self):
        return self._lamp('off', 'off', 'off')

    def full(self):
        return self._lamp('on', 'on', 'on')

    def good(self):
        return self._lamp('off', 'off', 'on')

    def fail(self):
        return self._lamp('on', 'off', 'off')

    def note(self):
        return self._lamp('off', 'on', 'off')

    def wait(self):
        return self._lamp('off', 'toggle', 'off')
