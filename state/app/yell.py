from logging import getLogger
from tkinter import Frame, Label, Tk


class Yell:
    def __init__(self, args):
        self._log = getLogger(self.__class__.__name__)
        self.args = args
        self.fail = set()
        self.good = set()

    @property
    def lines(self):
        return max(len(self.fail), len(self.good))

    def filled(self, *values):
        for value in sorted(values):
            yield value
        for _ in range(self.lines - len(values)):
            yield ''

    def _thing(self, master, color, column, *values):
        frame = Frame(master=master, bg=color)
        frame.pack_propagate(0)
        frame.grid(row=0, column=column)

        for row, value in enumerate(self.filled(*values)):
            label = Label(
                master=frame, bg=color, fg='#ffffff',
                text=value, font=('', 32),
            )
            label.grid(row=row, column=0, padx=10, pady=5)

    def __call__(self):
        if not self.lines:
            self._log.warning('there is nothing to display')
            return

        root = Tk()
        root.title('Build Status')
        root.config(bg='#333333')
        root.attributes("-topmost", True)

        column = 0
        for color, source in [
                ('#de4065', self.fail),
                ('#00b2a5', self.good),
        ]:
            if source:
                self._thing(root, color, column, *source)
                column += 1

        root.after(self.args.shout, root.destroy)
        self._log.info('showing window...')
        root.mainloop()
