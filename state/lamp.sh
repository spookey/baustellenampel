#!/usr/bin/env sh

PROBE=${PROBE-"http://probe.local"}
STATE=${STATE-"toggle"}

_lamp() {
    curl -X POST -fsS "$PROBE/light/$1/$STATE" > /dev/null 2>&1
}

for arg in "$@"; do
    for char in $(printf "%s\n" "$arg" | sed 's/./ &/g'); do
        case $char in
            r|R)    _lamp "r" ;;
            g|G)    _lamp "g" ;;
            y|Y)    _lamp "y" ;;
            *)      sleep .2  ;;
        esac
        sleep .5
    done
done

exit 0
