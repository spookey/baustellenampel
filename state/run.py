#!/usr/bin/env python3

from sys import exit as _exit

from app.main import run

if __name__ == '__main__':
    _exit(run())
