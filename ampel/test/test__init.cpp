#include "test.hpp"

void _init_CABLE_BAUDRT(void) {
    TEST_ASSERT_EQUAL(115200, CABLE_BAUDRT);
}
void _init_CABLE_CONFIG(void) {
    TEST_ASSERT_EQUAL(SERIAL_8N1, CABLE_CONFIG);
}

void _init_COVER_H_NAME(void) {
    TEST_ASSERT_EQUAL_STRING("device", COVER_H_NAME);
}
void _init_COVER_REPEAT(void) {
    TEST_ASSERT_EQUAL(4, COVER_REPEAT);
}
void _init_COVER_REPLAY(void) {
    TEST_ASSERT_EQUAL(240000, COVER_REPLAY);
}
void _init_COVER_W_PASS(void) {
    TEST_ASSERT_EQUAL_STRING("secret", COVER_W_PASS);
}
void _init_COVER_W_SSID(void) {
    TEST_ASSERT_EQUAL_STRING("aether", COVER_W_SSID);
}

void _init_FLASH_CONFIG(void) {
    TEST_ASSERT_EQUAL_STRING("/config_file", FLASH_CONFIG);
}
void _init_FLASH_INILEN(void) {
    TEST_ASSERT_EQUAL(16, FLASH_INILEN);
}
void _init_FLASH_NOTDEF(void) {
    TEST_ASSERT_EQUAL_STRING("?!?", FLASH_NOTDEF);
}
void _init_FLASH_RASTER(void) {
    TEST_ASSERT_EQUAL(16, FLASH_RASTER);
}
void _init_FLASH_VOIDER(void) {
    TEST_ASSERT_EQUAL_STRING("c[_]", FLASH_VOIDER);
}

void _init_LIGHT_NUMBER(void) {
#if VARIANT_LITE
    TEST_ASSERT_EQUAL(2, LIGHT_NUMBER);
#else
    TEST_ASSERT_EQUAL(3, LIGHT_NUMBER);
#endif
}
void _init_LIGHT_NOTDEF(void) {
    TEST_ASSERT_EQUAL_STRING("unknown", LIGHT_NOTDEF);
}
void _init_LIGHT_PIN_GG(void) {
    TEST_ASSERT_EQUAL(D5, LIGHT_PIN_GG);
}
void _init_LIGHT_PIN_RR(void) {
    TEST_ASSERT_EQUAL(D6, LIGHT_PIN_RR);
}
void _init_LIGHT_PIN_YY(void) {
    TEST_ASSERT_EQUAL(D7, LIGHT_PIN_YY);
}

void _init_SERVE_HARBOR(void) {
    TEST_ASSERT_EQUAL(80, SERVE_HARBOR);
}
void _init_SERVE_UPDATE(void) {
    TEST_ASSERT_EQUAL(120000, SERVE_UPDATE);
}void _init_SHELL_CMDLEN(void) {
    TEST_ASSERT_EQUAL(16, SHELL_CMDLEN);
}

void _init_SHELL_PREFIX(void) {
    TEST_ASSERT_EQUAL_STRING("C:\\>", SHELL_PREFIX);
}
void _init_SHELL_PROMPT(void) {
    TEST_ASSERT_EQUAL(64, SHELL_PROMPT);
}

void _init__CHAR_IGNORE(void) {
    TEST_ASSERT_EQUAL('\0', _CHAR_IGNORE);
}
void _init__CODE_BACKSP(void) {
    TEST_ASSERT_EQUAL(8, _CODE_BACKSP);
}
void _init__CODE_ESCAPE(void) {
    TEST_ASSERT_EQUAL(27, _CODE_ESCAPE);
}
void _init__CODE_FILLER(void) {
    TEST_ASSERT_EQUAL(32, _CODE_FILLER);
}
void _init__CODE_RETURN(void) {
    TEST_ASSERT_EQUAL(13, _CODE_RETURN);
}

void test__init(void) {
    RUN_TEST(_init_CABLE_BAUDRT);
    RUN_TEST(_init_CABLE_CONFIG);

    RUN_TEST(_init_COVER_H_NAME);
    RUN_TEST(_init_COVER_REPEAT);
    RUN_TEST(_init_COVER_REPLAY);
    RUN_TEST(_init_COVER_W_PASS);
    RUN_TEST(_init_COVER_W_SSID);

    RUN_TEST(_init_FLASH_CONFIG);
    RUN_TEST(_init_FLASH_INILEN);
    RUN_TEST(_init_FLASH_NOTDEF);
    RUN_TEST(_init_FLASH_RASTER);
    RUN_TEST(_init_FLASH_VOIDER);

    RUN_TEST(_init_LIGHT_NUMBER);
    RUN_TEST(_init_LIGHT_NOTDEF);
    RUN_TEST(_init_LIGHT_PIN_GG);
    RUN_TEST(_init_LIGHT_PIN_RR);
    RUN_TEST(_init_LIGHT_PIN_YY);

    RUN_TEST(_init_SERVE_HARBOR);
    RUN_TEST(_init_SERVE_UPDATE);

    RUN_TEST(_init_SHELL_CMDLEN);
    RUN_TEST(_init_SHELL_PREFIX);
    RUN_TEST(_init_SHELL_PROMPT);

    RUN_TEST(_init__CHAR_IGNORE);
    RUN_TEST(_init__CODE_BACKSP);
    RUN_TEST(_init__CODE_ESCAPE);
    RUN_TEST(_init__CODE_FILLER);
    RUN_TEST(_init__CODE_RETURN);
}
