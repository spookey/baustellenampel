#include "test.hpp"

class TestServe : public Serve {
public:
    TestServe(void) : Serve(get_cable(), get_shell(), get_cover(), get_light()) {};
    uint32_t get_requests(void) { return this->requests; }
};

void serve_counters(void) {
    TestServe web = TestServe();

    TEST_ASSERT_EQUAL(0, web.get_requests());
}


void test_serve(void) {
    RUN_TEST(serve_counters);
}
