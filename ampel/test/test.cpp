#include "test.hpp"

Cable txt = Cable();
Cable& get_cable(void) { return txt; }

Shell exe = Shell(txt);
Shell& get_shell(void) { return exe; }

Flash ini = Flash(txt, exe, FLASH_TESTFN);
Flash& get_flash(void) { return ini; }

Cover net = Cover(txt, exe, ini);
Cover& get_cover(void) { return net; }

Light led = Light(txt, exe);
Light& get_light(void) { return led; }

Serve web = Serve(txt, exe, net, led);


void setup(void) {
    delay(PRE_DELAY);
    UNITY_BEGIN();

    test__init();
    test_cable();
    test_shell();
    test_flash();
    test_cover();
    test_light();
    test_serve();

    UNITY_END();
}

void loop(void) {}
