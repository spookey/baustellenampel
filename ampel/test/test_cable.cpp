#include "test.hpp"

class TestCable : public Cable {
public:
    TestCable(void) : Cable() {};
};

void cable_text_fill(void) {
    TestCable txt = TestCable();

    TEST_ASSERT_EQUAL_STRING(
        "        ",
        txt.fill().c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        " ", txt.fill(1).c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "####", txt.fill(4, '#').c_str()
    );
}
void cable_text_join(void) {
    TestCable txt = TestCable();

    TEST_ASSERT_EQUAL_STRING(
        "WurstWasser", txt.join("Wurst", "Wasser").c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "GrobeKalbsLeberWurst", txt.join("Grobe", "Kalbs", "Leber", "Wurst").c_str()
    );
}
void cable_text_pad(void) {
    TestCable txt = TestCable();

    TEST_ASSERT_EQUAL_STRING(
        "..", txt.pad("aaa", true, 2).c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "..", txt.pad("aaa", false, 2).c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "##", txt.pad("aaa", true, 2, ' ', '#').c_str()
    );

    TEST_ASSERT_EQUAL_STRING(
        "  aaaaaa", txt.pad("aaaaaa", true).c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "aaaaaa  ", txt.pad("aaaaaa", false).c_str()
    );

    TEST_ASSERT_EQUAL_STRING(
        "  aaaaaa", txt.pad("aaaaaa", true, 8).c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "##aaaaaa", txt.pad("aaaaaa", true, 8, '#').c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "aaaaaa##", txt.pad("aaaaaa", false, 8, '#').c_str()
    );

    TEST_ASSERT_EQUAL_STRING(
        "aa..", txt.pad("aaaaaa", true, 4, '#').c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "aa??", txt.pad("aaaaaa", true, 4, '#', '?').c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "aa??", txt.pad("aaaaaa", false, 4, '#', '?').c_str()
    );
}

void cable_log_str(void) {
    TestCable txt = TestCable();

    TEST_ASSERT_EQUAL_STRING(
        "\nALARM      gefahr", txt.log_str("alarm", "gefahr").c_str()
    );
}
void cable_llg_str(void) {
    TestCable txt = TestCable();

    TEST_ASSERT_EQUAL_STRING(
        "           alarm: gefahr", txt.llg_str("alarm", "gefahr").c_str()
    );
    TEST_ASSERT_EQUAL_STRING(
        "    lecker wurst: wasser", txt.llg_str("lecker wurst", "wasser").c_str()
    );
}


void test_cable(void) {
    RUN_TEST(cable_text_fill);
    RUN_TEST(cable_text_join);
    RUN_TEST(cable_text_pad);
    RUN_TEST(cable_log_str);
    RUN_TEST(cable_llg_str);
}
