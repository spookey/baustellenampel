#include "test.hpp"

class TestFlash : public Flash {
public:
    TestFlash(void) : Flash(get_cable(), get_shell(), FLASH_TESTFN) {};
    bool run_add(String key, String val) { return this->add(key, val); }
    bool run_set(String key, String value) { return this->set(key, value); };
};

void flash_get_fallback(void) {
    TestFlash ini = TestFlash();

    TEST_ASSERT_EQUAL_STRING("test", ini.get("unit", "test", false).c_str());
}

void flash_add_and_read(void) {
    TestFlash ini = TestFlash();

    TEST_ASSERT_EQUAL(false, ini.run_add("", "aaa"));
    TEST_ASSERT_EQUAL(false, ini.run_add(" ", "bbb"));
    TEST_ASSERT_EQUAL(true, ini.run_add("unit", "test"));
    TEST_ASSERT_EQUAL_STRING("test", ini.get("unit", "ccc", false).c_str());
}

void flash_add_and_set(void) {
    TestFlash ini = TestFlash();

    TEST_ASSERT_EQUAL(true, ini.run_add("unit", "test"));
    TEST_ASSERT_EQUAL_STRING("test", ini.get("unit", "aaa", false).c_str());
    TEST_ASSERT_EQUAL(true, ini.run_set("unit", "demo"));
    TEST_ASSERT_EQUAL_STRING("demo", ini.get("unit", "bbb", false).c_str());
}


void test_flash(void) {
    RUN_TEST(flash_get_fallback);
    RUN_TEST(flash_add_and_read);
    RUN_TEST(flash_add_and_set);
}
