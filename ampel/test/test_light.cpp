#include "test.hpp"

class TestLight: public Light {
public:
    TestLight(void) : Light(get_cable(), get_shell()) {};
};

void light_lamp_names(void) {
    TestLight led = TestLight();

    TEST_ASSERT_EQUAL_STRING("green", led.get_name(0).c_str());
    TEST_ASSERT_EQUAL_STRING("red", led.get_name(1).c_str());
#if !VARIANT_LITE
    TEST_ASSERT_EQUAL_STRING("yellow", led.get_name(2).c_str());
#endif

    TEST_ASSERT_EQUAL_STRING("", led.get_name(3).c_str());
}

void light_initial_true(void) {
    TestLight led = TestLight();

    for (uint8_t idx = 0; idx < LIGHT_NUMBER; idx++) {
        TEST_ASSERT_EQUAL(true, led.get_state(idx));
    }

    TEST_ASSERT_EQUAL(false, led.get_state(1 + LIGHT_NUMBER));
}

void light_rounds(void) {
    TestLight led = TestLight();

    for (uint8_t idx = 0; idx < LIGHT_NUMBER; idx++) {
        TEST_ASSERT_EQUAL(true, led.off(idx));
        TEST_ASSERT_EQUAL(true, led.on(idx));
        TEST_ASSERT_EQUAL(true, led.toggle(idx));
    }

    TEST_ASSERT_EQUAL(false, led.off(1 + LIGHT_NUMBER));
    TEST_ASSERT_EQUAL(false, led.on(1 + LIGHT_NUMBER));
    TEST_ASSERT_EQUAL(false, led.toggle(1 + LIGHT_NUMBER));
}


void test_light(void) {
    RUN_TEST(light_lamp_names);
    RUN_TEST(light_initial_true);
    RUN_TEST(light_rounds);
}
