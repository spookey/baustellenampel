#include "test.hpp"

class TestShell: public Shell {
public:
    TestShell() : Shell(get_cable()) {};
    uint8_t get_cmd_idx(void) { return this->cmd_idx; };
    void run_remove(bool full) { this->_remove(full); };
    void run_collect(char data) { this->_collect(data); };
    void run_enter(void) { this->_enter(); };
    String get_input(void) { return this->input; };
};

void shell_get_launched(void) {
    TestShell exe = TestShell();

    TEST_ASSERT_EQUAL(0, exe.get_launched());
    TEST_ASSERT_EQUAL(0, exe.get_cmd_idx());

    exe.run_enter();
    TEST_ASSERT_EQUAL(0, exe.get_launched());

    exe.run_collect('.');
    exe.run_enter();
    TEST_ASSERT_EQUAL(1, exe.get_launched());
}

void shell_text_input(void) {
    TestShell exe = TestShell();

    TEST_ASSERT_EQUAL_STRING("", exe.get_input().c_str());

    exe.run_collect('H'); exe.run_collect('e'); exe.run_collect('l');
    exe.run_collect('p'); exe.run_collect('!'); exe.run_collect('!');
    TEST_ASSERT_EQUAL_STRING("Help!!", exe.get_input().c_str());

    exe.run_remove(false);
    exe.run_remove(false);
    TEST_ASSERT_EQUAL_STRING("Help", exe.get_input().c_str());

    exe.run_remove(true);
    TEST_ASSERT_EQUAL_STRING("", exe.get_input().c_str());
}

void shell_enter_clears(void) {
    TestShell exe = TestShell();

    TEST_ASSERT_EQUAL_STRING("", exe.get_input().c_str());
    exe.run_collect('.');
    TEST_ASSERT_EQUAL_STRING(".", exe.get_input().c_str());
    exe.run_enter();
    TEST_ASSERT_EQUAL_STRING("", exe.get_input().c_str());
}


void test_shell(void) {
    RUN_TEST(shell_get_launched);
    RUN_TEST(shell_text_input);
    RUN_TEST(shell_enter_clears);
}
