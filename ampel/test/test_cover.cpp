#include "test.hpp"

class TestCover : public Cover {
public:
    TestCover(void) : Cover(get_cable(), get_shell(), get_flash()) {};
    uint8_t get_repeat(void) { return this->repeat; }
    unsigned long get_replay(void) { return this->replay; }
};

void cover_initial_setup(void) {
    TestCover net = TestCover();

    TEST_ASSERT_EQUAL(0, net.get_dialups());
    TEST_ASSERT_EQUAL(0, net.get_hangups());
    TEST_ASSERT_EQUAL(COVER_REPEAT, net.get_repeat());
    TEST_ASSERT_NOT_EQUAL(0, net.get_replay());
    TEST_ASSERT_LESS_OR_EQUAL(millis(), net.get_replay());
}

void cover_hostname(void) {
    TestCover net = TestCover();

    TEST_ASSERT_EQUAL_STRING(
        wifi_station_get_hostname(), net.get_hostname().c_str()
    );
}

void cover_signal(void) {
    TestCover net = TestCover();
    String signal = net.get_signal();
    const uint16_t len = signal.length();

    TEST_ASSERT_INT_WITHIN(90, -45, signal.substring(0, 4).toInt());
    TEST_ASSERT_EQUAL_STRING(" dBm", signal.substring(len - 4).c_str());
}

void cover_channel(void) {
    TestCover net = TestCover();
    String channel = net.get_channel();

    TEST_ASSERT_EQUAL_STRING("#", channel.substring(0, 1).c_str());
    // channel 14 occurs when not connected
    TEST_ASSERT_UINT_WITHIN(14, 0, channel.substring(1).toInt());
}


void test_cover(void) {
    RUN_TEST(cover_initial_setup);
    RUN_TEST(cover_hostname);
    RUN_TEST(cover_signal);
    RUN_TEST(cover_channel);
}
