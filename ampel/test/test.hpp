#ifndef ___test_init__
#define ___test_init__

#include <Arduino.h>
#include <unity.h>

#define PRE_DELAY           2500            // ms pause before running tests
#define FLASH_TESTFN        "/config_test"  // config filename

#include "_init.hpp"
#include "cable.hpp"
#include "shell.hpp"
#include "flash.hpp"
#include "cover.hpp"
#include "light.hpp"
#include "serve.hpp"

void test__init(void);
void test_cable(void);
void test_shell(void);
void test_flash(void);
void test_cover(void);
void test_light(void);
void test_serve(void);

Cable& get_cable(void);
Shell& get_shell(void);
Flash& get_flash(void);
Cover& get_cover(void);
Light& get_light(void);

#endif
