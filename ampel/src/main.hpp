#ifndef __main_hpp__
#define __main_hpp__

#include "_init.hpp"
#include "cable.hpp"
#include "shell.hpp"
#include "flash.hpp"
#include "cover.hpp"
#include "light.hpp"
#include "serve.hpp"

void setup(void);
void loop(void);

#endif
