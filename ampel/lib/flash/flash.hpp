#ifndef __flash_hpp__
#define __flash_hpp__

#include <FS.h>

#include "_init.hpp"
#include "cable.hpp"
#include "shell.hpp"

class Flash {
public:
    Flash(Cable& txt, Shell& exe);
    Flash(Cable& txt, Shell& exe, String filename);
    ~Flash(void);
    void setup(void);

protected:
    Cable& txt;
    Shell& exe;
    String filename = FLASH_CONFIG;

private:
    struct Blob { String key; String val; };
    uint8_t index = 0;
    Blob* items = new Blob[FLASH_INILEN];

protected:
    bool dump(const bool action=true);
    bool load(const bool action=true);

    String pickle(Blob data);
    String pickle(void);
    bool unpickle(String line);

    bool add(String key, String value);
    bool set(String key, String value);

private:
    uint8_t cmd_confdrop(String _);
    uint8_t cmd_confdump(String _);
    uint8_t cmd_confload(String _);
    uint8_t cmd_confshow(String _);
    uint8_t cmd_conf(String text);

public:
    String get(String key, String fallback="", const bool create=false);

};

#endif
