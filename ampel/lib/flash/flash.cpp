#include "flash.hpp"

Flash::Flash(Cable& txt, Shell& exe)
: txt(txt), exe(exe)
{}
Flash::Flash(Cable& txt, Shell& exe, String filename)
: txt(txt), exe(exe), filename(filename)
{}
Flash::~Flash(void) {
    delete[] this->items;
}

void Flash::setup(void) {
    if (!SPIFFS.begin()) { this->txt.sos("flash error", true); }
    this->load();
    this->exe.add(this, &Flash::cmd_confdrop, "cdrop", "drop config file");
    this->exe.add(this, &Flash::cmd_confdump, "cdump", "config -> file");
    this->exe.add(this, &Flash::cmd_confload, "cload", "file -> config");
    this->exe.add(this, &Flash::cmd_confshow, "cshow", "show config file");
    this->exe.add(this, &Flash::cmd_conf, "conf", "config utility");
}


bool Flash::dump(const bool action) {
    uint32_t size = 0;
    String conf = "";
    this->txt.log("flash", "dump");

    if (action) {
        conf = this->pickle();
        size = conf.length();
        if (!size) {
            this->txt.llg("no input", "abort");
            return false;
        }
        this->txt.llg("input size", this->txt.join(String(size, DEC), " bytes"));
    }

    this->txt.llg("filename", this->filename);
    this->txt.llg("exists", (SPIFFS.exists(this->filename) ? "yes" : "no"));

    File file = SPIFFS.open(this->filename, "w+");
    if (!file) {
        this->txt.llg("open", "failed");
        return false;
    }
    if (!file.seek(0, SeekSet)) {
        this->txt.llg("rewind", "failed");
        return false;
    }

    this->txt.llg("ready", "writing ->");
    this->txt.text(conf, true);
    size = file.println(conf);
    file.close();
    this->txt.llg("written", this->txt.join(String(size, DEC), " bytes"));
    return true;
}

String Flash::pickle(Blob data) {
    return this->txt.join(
        this->txt.pad(data.key, false, FLASH_RASTER, ' ', '%'),
        this->txt.pad(data.val, true, FLASH_RASTER, ' ', '%')
    );
}
String Flash::pickle(void) {
    String conf = this->txt.join("# created - ", String(millis(), DEC));
    for (uint8_t idx = 0; idx < this->index; idx++) {
        conf = this->txt.join(conf, "\n", this->pickle(this->items[idx]));
    }
    return this->txt.join(conf, "\n");
}

bool Flash::load(const bool action) {
    String line = "";
    this->txt.log("flash", "load");
    this->txt.llg("filename", this->filename);
    if (!SPIFFS.exists(this->filename)) {
        this->txt.llg("does not exist", "abort");
        return false;
    }

    File file = SPIFFS.open(this->filename, "r");
    if (!file) {
        this->txt.llg("open", "failed");
        return false;
    }
    if (!file.seek(0, SeekSet)) {
        this->txt.llg("rewind", "failed");
        return false;
    }

    this->txt.llg("ready", "reading ->");
    while (file.available()) {
        line = file.readStringUntil('\n');
        if (action) {
            this->txt.llg("->", (this->unpickle(line) ? "ok" : "skipped"));
        } else {
            this->txt.text(line, true);
        }
    }
    file.close();
    return true;
}

bool Flash::unpickle(String line) {
    line.trim();
    if(line.length() < (2 * FLASH_RASTER)) {
        return false;
    }
    if (line.startsWith("#")) {
        return false;
    }

    String key = line.substring(0, FLASH_RASTER);
    String val = line.substring(1 + FLASH_RASTER);
    key.trim();
    val.trim();
    if(!key.length() || !val.length()) {
        return false;
    }
    return this->add(key, val);
}

bool Flash::add(String key, String val) {
    key.trim();
    if (!key.length()) {
        return false;
    }
    if (this->set(key, val)) {
        return true;
    }

    if (this->index < FLASH_INILEN) {
        this->items[this->index] = Blob({key, val});
        this->index++;
        return true;
    }
    return false;
}

bool Flash::set(String key, String val) {
    for (uint8_t idx = 0; idx < this->index; idx++) {
        if (this->items[idx].key == key) {
            this->items[idx].val = val;
            return true;
        }
    }
    return false;
}

String Flash::get(String key, String fallback, const bool create) {
    for (uint8_t idx = 0; idx < this->index; idx++) {
        if (this->items[idx].key == key) {
            return this->items[idx].val;
        }
    }
    if (create) {
        if (!this->add(key, fallback)) {
            return "";
        }
    }
    return fallback;
}


uint8_t Flash::cmd_confdrop(String _) {
    this->txt.log("flash", "conf drop");
    return (this->dump(false) ? 0 : 1);
}
uint8_t Flash::cmd_confdump(String _) {
    this->txt.log("flash", "conf dump");
    return (this->dump(true) ? 0 : 1);
}
uint8_t Flash::cmd_confload(String _) {
    this->txt.log("flash", "conf load");
    return (this->load(true) ? 0 : 1);
}
uint8_t Flash::cmd_confshow(String _) {
    this->txt.log("flash", "conf show");
    return (this->load(false) ? 0 : 1);
}
uint8_t Flash::cmd_conf(String text) {
    text.trim();
    String key = text;
    String val = "";

    const int16_t space = text.indexOf(' ');
    if (space > 0) {
        key = text.substring(0, space);
        val = text.substring(1 + space);
        key.trim();
        val.trim();
    }

    if (key.length() && val.length()) {
        this->txt.log("flash", "conf set");
        this->txt.llg(key, val);

        if (val == FLASH_VOIDER) {
            this->txt.llg("voiding", key);
            val = "";
        }

        if (!this->set(key, val)) {
            return 1;
        }
        this->txt.llg("state", "ok");
        return 0;
    }
    if (key.length()) {
        this->txt.log("flash", "conf get");
        val = this->get(key, FLASH_NOTDEF, false);
        this->txt.llg(key, val);
        if (val == FLASH_NOTDEF) {
            return 1;
        }
        return 0;
    }

    this->txt.log("flash", "conf all");
    this->txt.llg("items", String(this->index, DEC));
    this->txt.llg("free", String(FLASH_INILEN - this->index, DEC));
    this->txt.llg("void", FLASH_VOIDER);
    this->txt.llg(":", this->txt.join("--", " ::"));
    for (uint8_t idx = 0; idx < this->index; idx++) {
        this->txt.llg(this->items[idx].key, this->items[idx].val);
    }
    return 0;
}
