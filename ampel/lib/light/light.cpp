#include "light.hpp"

Light::Light(Cable& txt, Shell& exe)
: txt(txt), exe(exe)
{}
Light::~Light(void) {
    delete[] this->lamps;
}

Light::Lamp::Lamp(Cable& txt, const uint8_t pin, String name)
: txt(txt), pin(pin), name(name), state(true)
{}

void Light::setup(void) {
    for (uint8_t idx = 0; idx < LIGHT_NUMBER; idx++) {
        pinMode(this->lamps[idx].pin, OUTPUT);
        this->lamps[idx].off();
    }
    this->exe.add(this, &Light::cmd_sos, "sos", "run sos");
    this->exe.add(this, &Light::cmd_lamp, "lamp", "control lamps");
}

bool Light::Lamp::toggle(void) {
    this->txt.log("light", "toggle");
    this->txt.llg("color", this->name);
    this->txt.llg("state", this->state ? "off" : "on");
    digitalWrite(this->pin, this->state ? HIGH : LOW);
    this->state = !this->state;
    return true;
}

String Light::get_name(const uint8_t num) {
    if (this->valid(num)) {
        return this->lamps[num].name;
    }
    return "";
}
bool Light::get_state(const uint8_t num) {
    if (this->valid(num)) {
        return this->lamps[num].state;
    }
    return false;
}

bool Light::toggle(const uint8_t num) {
    if (this->valid(num)) {
        return this->lamps[num].toggle();
    }
    return false;
}
bool Light::off(const uint8_t num) {
    if (this->valid(num)) {
        return this->lamps[num].off();
    }
    return false;
}
bool Light::on(const uint8_t num) {
    if (this->valid(num)) {
        return this->lamps[num].on();
    }
    return false;
}


uint8_t Light::cmd_sos(String reason) {
    reason.trim();
    if (!reason.length()) {
        reason = "manual";
    }
    this->txt.sos(reason, false);
    return 0;
}

uint8_t Light::cmd_lamp(String args) {
    args.trim();
    args.toLowerCase();
    uint16_t len = args.length();
    if (!len) {
        return 1;
    }
    for (uint16_t idx = 0; idx < len; idx++) {
        switch (args.charAt(idx)) {
            case 'g':   this->lamps[0].toggle();    break;
            case 'r':   this->lamps[1].toggle();    break;
#if !VARIANT_LITE
            case 'y':   this->lamps[2].toggle();    break;
#endif
            default:    delay(256);                 break;
        }
        delay(64);
    }
    return 0;
}
