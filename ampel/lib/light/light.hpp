#ifndef __light_hpp__
#define __light_hpp__

#include "_init.hpp"
#include "cable.hpp"
#include "shell.hpp"

class Light {
public:
    Light(Cable& txt, Shell& exe);
    ~Light(void);
    void setup(void);

protected:
    Cable& txt;
    Shell& exe;

protected:
    struct Lamp {
        Cable& txt;
        const uint8_t pin;
        String name;
        bool state;

        Lamp(Cable& txt, const uint8_t pin, String name);
        bool toggle(void);
        bool off(void) { this->state = true; return this->toggle(); }
        bool on(void) { this->state = false; return this->toggle(); }
    };
    Lamp* lamps = new Lamp[LIGHT_NUMBER] {
        Lamp(this->txt, LIGHT_PIN_GG, "green"),
        Lamp(this->txt, LIGHT_PIN_RR, "red"),
#if !VARIANT_LITE
        Lamp(this->txt, LIGHT_PIN_YY, "yellow"),
#endif
    };

public:
    bool valid(const uint8_t num) {
        return num >= 0 && num < LIGHT_NUMBER;
    }

    String get_name(const uint8_t num);
    bool get_state(const uint8_t num);

    bool toggle(const uint8_t num);
    bool off(const uint8_t num);
    bool on(const uint8_t num);

private:
    uint8_t cmd_sos(String reason);
    uint8_t cmd_lamp(String args);
};

#endif
