#include "shell.hpp"

Shell::Shell(Cable& txt)
: txt(txt)
{}
Shell::~Shell(void) {
    for (uint8_t idx = 0; idx < this->cmd_idx; idx++) {
        delete &this->items[idx];
    }
    delete[] this->items;
}

bool Shell::add(Command& cmd) {
    for (uint8_t idx = 0; idx < SHELL_CMDLEN; idx++) {
        if (&this->items[idx] == &cmd) {
            return true;
        }
    }
    if (this->cmd_idx < SHELL_CMDLEN) {
        this->items[this->cmd_idx] = cmd;
        this->cmd_idx++;
        return true;
    }
    return false;
}

void Shell::help(void) {
    this->txt.log("shell", "commands");
    this->txt.llg("items", String(this->cmd_idx, DEC));
    this->txt.llg("free", String(SHELL_CMDLEN - this->cmd_idx, DEC));
    this->txt.llg(":", this->txt.join("#", String(this->launched, DEC), " ::"));
    for (uint8_t idx = 0; idx < this->cmd_idx; idx++) {
        this->txt.llg(this->items[idx].name, this->items[idx].help);
    }
}

void Shell::launch(String line) {
    String name = "";
    String arguments = "";
    this->launched++;
    uint8_t code = 1;
    for (uint8_t idx = 0; idx < this->cmd_idx; idx++) {
        name = this->items[idx].name;
        if (name == line.substring(0, name.length())) {
            arguments = line.substring(1 + name.length());
            arguments.trim();
            code = this->items[idx].call(arguments);
            if (code != 0) {
                this->txt.log("shell", this->txt.join("[", line, "]"));
                this->txt.llg("code", String(code, DEC));
            }
            return;
        }
    }
    this->help();
}

void Shell::_intro(void) {
    String prefix = SHELL_PREFIX;
    this->_remove(true);
    this->txt.text(this->txt.pad(
        this->txt.join("\n\r", prefix), false, 3 + prefix.length(), ' ', '%'
    ), false);
}
void Shell::_remove(const bool full) {
    while (this->input.length()) {
        this->input.remove(this->input.length() - 1, 1);
        this->txt.raw(_CODE_BACKSP);
        this->txt.raw(_CODE_FILLER);
        this->txt.raw(_CODE_BACKSP);
        if (!full) { return; }
    }
}
void Shell::_collect(const char entry) {
    if (entry > 31 && entry < 127) {
        if (this->input.length() <= SHELL_PROMPT) {
            this->input.concat(entry);
            this->txt.raw(entry);
            this->txt.raw(_CODE_BACKSP);
            this->txt.raw(entry);
        }
    }
}
void Shell::_enter(void) {
    String line = "";
    if (this->input.length() > 0) {
        line = this->input;
        line.trim();
    }
    if (line.length() > 0) {
        this->launch(line);
    }
    this->_intro();
}

void Shell::loop(void) {
    const char entry = this->txt.collect();
    switch (entry) {
        case _CODE_BACKSP:      this->_remove(false);           break;
        case _CODE_ESCAPE:      this->_remove(true);            break;
        case _CODE_RETURN:      this->_enter();                 break;
        default:                this->_collect(entry);          break;
    }
}
