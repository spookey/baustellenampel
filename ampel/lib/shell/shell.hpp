#ifndef __shell_hpp__
#define __shell_hpp__

#include "_init.hpp"
#include "cable.hpp"

class Shell {
public:
    Shell(Cable& txt);
    ~Shell(void);
    void loop(void);

protected:
    Cable& txt;
    uint8_t cmd_idx = 0;
    uint32_t launched = 0;

private:
    struct Command {
        void *base;
        char ident[0x21];
        uint8_t (*caller)(void*, char*, String);
        String name;
        String help;

        uint8_t call(String text) {
            return this->caller(this->base, this->ident, text);
        }

        template<typename T>
        static uint8_t invoke(void *base, char *ident, String text) {
            T *obj = static_cast<T*>(base);
            uint8_t (T::*func)(String);
            memcpy((char*) &func, ident, sizeof(func));
            return (obj->*func)(text);
        }
    };

    bool add(Command& cmd);

public:
    template<typename T>
    bool add(
        T* base, uint8_t (T::*ident)(String), String name, String help="-"
    ) {

        Command* cmd = new Command;
        cmd->base = static_cast<void*>(base);
        memcpy(cmd->ident, (char*) &ident, sizeof(ident));
        cmd->caller = &Command::invoke<T>;
        cmd->name = name;
        cmd->help = help;

        return this->add(*cmd);
    }

protected:
    void help(void);
    void launch(String line);

    void _intro(void);
    void _remove(const bool full);
    void _collect(const char entry);
    void _enter(void);

    String input = "";
    Command* items = new Command[SHELL_CMDLEN];

public:
    uint32_t get_launched(void) { return this->launched; }
};

#endif
