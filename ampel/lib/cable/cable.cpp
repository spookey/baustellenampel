#include "cable.hpp"

void Cable::setup(void) {
#ifndef UNIT_TEST
    Serial.begin(CABLE_BAUDRT, CABLE_CONFIG);
    do { delay(.125); } while (!Serial);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    this->text(this->fill(0xff, '%'), true);
#endif
}

char Cable::collect(void) {
#ifndef UNIT_TEST
    const int16_t val = Serial.read();
    if (val >= 0) { return (char) val; }
#endif
    return _CHAR_IGNORE;
}

void Cable::raw(const char data) {
#ifndef UNIT_TEST
    Serial.write(data);
#endif
}
void Cable::text(String text, const bool newline) {
#ifndef UNIT_TEST
    (newline ? Serial.println(text) : Serial.print(text));
    Serial.flush();
#endif
}


String Cable::fill(const uint8_t width, const char character) {
    String result = "";
    result.reserve(width);
    for (uint8_t idx = 0; idx < width; idx++) {
        result.concat(character);
    }
    return result;
}

String Cable::join(String aa, String bb) {
    String result = "";
    result.reserve(aa.length() + bb.length());
    result.concat(aa);
    result.concat(bb);
    return result;
}

String Cable::pad(
        String text,
        const bool pre, const uint8_t width,
        const char filler, const char skipper
) {
    if (width <= 2) {
        return this->fill(width, skipper);
    }
    const uint32_t len = text.length();
    if (width >= len) {
        String fill = this->fill(width - len, filler);
        return (pre ? this->join(fill, text) : this->join(text, fill));
    }
    return text.substring(0, width - 2) + this->fill(2, skipper);
}


String Cable::log_str(String name, String text) {
    name.toUpperCase();
    return this->join("\n", this->pad(name, false, 10), " ", text);
}

String Cable::llg_str(String name, String text) {
    return this->join(this->pad(name, true, 16), ": ", text);
}

void Cable::sos(String reason, const bool infinite, const unsigned long wait) {
    String dit = this->join(this->fill(0xff, ':'), this->fill(0xff, ':'));
    String dah = this->join(dit, dit, dit, dit);
    while (infinite || this->collect() != _CHAR_IGNORE) {
        this->text("", true);
        for (uint8_t idx = 0; idx <= 2; idx++) {
            for (uint8_t _ = 0; _ <= 2; _++) {
                this->text((idx == 1) ? dah : dit, false);
                digitalWrite(LED_BUILTIN, LOW);
                delay(512);
                digitalWrite(LED_BUILTIN, HIGH);
            }
            digitalWrite(LED_BUILTIN, LOW);
            delay(512);
            digitalWrite(LED_BUILTIN, HIGH);
        }
        this->log("cable", "alarm");
        this->llg("reason", reason);
        delay(wait);
    }
}

String Cable::get_uptime(void) {
    unsigned long sec = millis() / 1000;
    unsigned long min = sec / 60;
    unsigned long hrs = min / 60;
    unsigned long day = hrs / 24;
    sec = sec - (min * 60);
    min = min - (hrs * 60);
    hrs = hrs - (day * 24);
    static char res[16];
    if (day >= 1) { snprintf(res, 16, "%ldd %02ld:%02ld:%02ld", day, hrs, min, sec); }
    else {          snprintf(res, 16,      "%02ld:%02ld:%02ld",      hrs, min, sec); }
    return String(res);
}
