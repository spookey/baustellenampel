#ifndef __cable_hpp__
#define __cable_hpp__

#include "_init.hpp"

class Cable {
public:
    Cable(void) {};
    void setup(void);

public:
    char collect(void);
    void raw(const char data);
    void text(String text, const bool newline=false);

    String fill(const uint8_t width=8, const char character=' ');
    String join(String aa, String bb);
    String pad(
            String text, const bool pre=true, const uint8_t width=8,
            const char filler=' ', const char skipper='.'
    );

    void sos(
        String reason, const bool infinite=true,
        const unsigned long wait=2048
    );

    void log(String name, String text) {
        this->text(this->log_str(name, text), true);
    }
    void llg(String name, String text) {
        this->text(this->llg_str(name, text), true);
    }

public:
    String join(String aa, String bb, String cc) {
        return this->join(aa, this->join(bb, cc));
    };
    String join(String aa, String bb, String cc, String dd) {
        return this->join(this->join(aa, bb), this->join(cc, dd));
    };

    String log_str(String name, String text);
    String llg_str(String name, String text);

    String get_uptime(void);
};

#endif
