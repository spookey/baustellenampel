document.addEventListener('DOMContentLoaded', function() {
    function fetch(value) { return document.getElementById(value); }

    let tout = null;
    function message(msg) {
      if (tout) { clearTimeout(tout); }
      tout = setTimeout(function() {
        fetch('txt-status').innerHTML = '&nbsp;';
        tout = null;
      }, 3000);
      fetch('txt-status').innerHTML = msg;
    }
    function color() {
      function v() { return Math.floor(Math.random() * 255); }
      return 'rgb(' + v() + ', ' + v() + ', ' + v() + ')';
    }
    function check(data, callback, arg) {
      if (data !== undefined && data !== null) { callback(data, arg); }
    }
    function status(resp) {
      function set(val, name) { fetch('txt-' + name).innerHTML = val; }
      check(resp, function(data) {
        check(data.channel, set, 'channel');
        check(data.dialups, set, 'dialups');
        check(data.hangups, set, 'hangups');
        check(data.launched, set, 'launched');
        check(data.requests, set, 'requests');
        check(data.signal, set, 'signal');
        check(data.uptime, set, 'uptime');

        document.body.style.backgroundColor = color();
      });
      message('ok');
    }
    function query(endpoint, callback) {
      message('query ' + endpoint + ' &hellip;');
      const xhr = new XMLHttpRequest();
      xhr.open('GET', '/' + endpoint, true);
      xhr.onreadystatechange = function() {
        if (
          xhr.readyState === 4 &&
          xhr.status >= 200 && xhr.status < 300 &&
          xhr.responseText
        ) {
          callback(JSON.parse(xhr.responseText));
        } else {
          message('query ' + endpoint + ' error');
        }
      };
      xhr.send();
    }
    function light(event) {
      query('light/' + event.target.id.replace('-', '/'), function(resp) {
        check(resp, function(data) {
            message(data.action + ' - ' + data.name);
        });
      });
    }
    (function () {
      message('setup &hellip;');

      const inputs = document.getElementsByTagName('input');
      let idx = inputs.length;
      while(idx--) {
        inputs[idx].addEventListener('click', light);
      }

      setInterval(function() { query('stats', status); }, __update__);
      status(JSON.parse('__status__'));
    })();
  });
