#ifndef __serve_hpp__
#define __serve_hpp__

#include <ESP8266WebServer.h>

#include "_init.hpp"
#include "cable.hpp"
#include "shell.hpp"
#include "cover.hpp"
#include "light.hpp"

class Serve {
public:
    Serve(Cable& txt, Shell& exe, Cover& net, Light& led);
    ~Serve(void);
    void setup(void);
    void loop(void);

protected:
    Cable& txt;
    Shell& exe;
    Cover& net;
    Light& led;

    uint32_t requests = 0;

private:
    String _block(const uint8_t num);

    String index(void);
    String stats(const bool verbose);

private:
    ESP8266WebServer* webserver = new ESP8266WebServer(SERVE_HARBOR);

    class Index : public RequestHandler {
    public:
        Index(Serve &web);
    protected:
        Serve& web;

        const String _index = "/";
        const String _light = "/light/";
        const String _stats = "/stats";

    public:
        bool canHandle(HTTPMethod meth, String req);
        bool handle(ESP8266WebServer& webserver, HTTPMethod meth, String req);
        bool canUpload(String _) { return false; }
    };

private:
    struct Json {
        Cable& txt;
        const bool verbose;
        String store = "";

        Json(Cable& txt, const bool verbose=false);
        void add(String key, String val, const bool raw=false);
        void add(String key, const int32_t val) {
            this->add(key, String(val, DEC), true);
        }
        String show(void);
    };

private:
    uint8_t cmd_restart(String _);
    uint8_t cmd_stats(String args);
};

#endif
