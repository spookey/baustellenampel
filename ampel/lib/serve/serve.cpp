#include "serve.hpp"

#define _INCL_STRING(...)   #__VA_ARGS__

const String SERVE_BASIC = String(
#include "inc/_basic.html"
);
const String SERVE_BLOCK = String(
#include "inc/_block.html"
);
const String SERVE_FUNCT = String(
#include "inc/_funct.js"
);
const String SERVE_STYLE = String(
#include "inc/_style.css"
);

#undef _INCL_STRING

Serve::Serve(Cable& txt, Shell& exe, Cover& net, Light& led)
: txt(txt), exe(exe), net(net), led(led)
{
    this->webserver->addHandler(new Index(*this));
}
Serve::~Serve(void) {
    delete this->webserver;
}

Serve::Index::Index(Serve& web)
: web(web)
{}

Serve::Json::Json(Cable& txt, const bool verbose)
: txt(txt), verbose(verbose)
{}

void Serve::setup(void) {
    this->webserver->begin();
    this->exe.add(this, &Serve::cmd_restart, "restart", "system restart");
    this->exe.add(this, &Serve::cmd_stats, "stats", "system statistics");
}
void Serve::loop(void) {
    this->webserver->handleClient();
}

String Serve::_block(const uint8_t num) {
    String elem = SERVE_BLOCK;
    String name = this->led.get_name(num);
    elem.replace("__name__", name);
    elem.replace("__key__", name.substring(0, 1));
    return elem;
}

String Serve::index(void) {
    String block = "";
    block = this->txt.join(block, this->_block(1));
#if !VARIANT_LITE
    block = this->txt.join(block, this->_block(2));
#endif
    block = this->txt.join(block, this->_block(0));
    String index = SERVE_BASIC;
    index.replace("__funct__", SERVE_FUNCT);
    index.replace("__style__", SERVE_STYLE);
    index.replace("__block__", block);
    index.replace("__status__", this->stats(false));
    index.replace("__hostname__", this->net.get_hostname());
    index.replace("__update__", String(SERVE_UPDATE, DEC));
    return index;
}

String Serve::stats(const bool verbose) {
    Json json = Json(this->txt, verbose);
    json.add("green", this->led.get_state(0) ? "true" : "false", true);
    json.add("red", this->led.get_state(1) ? "true" : "false", true);
#if !VARIANT_LITE
    json.add("yellow", this->led.get_state(2) ? "true" : "false", true);
#endif
    json.add("dialups", this->net.get_dialups());
    json.add("hangups", this->net.get_hangups());
    json.add("launched", this->exe.get_launched());
    json.add("requests", this->requests);
    json.add("channel", this->net.get_channel());
    json.add("signal", this->net.get_signal());
    json.add("uptime", this->txt.get_uptime());
    return json.show();
}

bool Serve::Index::canHandle(HTTPMethod meth, String req) {
     if (meth == HTTP_GET && (req == this->_index || req == this->_stats)) {
        return true;
    }
    if (meth == HTTP_GET || meth == HTTP_POST) {
        if (req.startsWith(this->_light)) {
            req = req.substring(this->_light.length());
#if VARIANT_LITE
            if (req.startsWith("g/") || req.startsWith("r/")) {
#else
            if (req.startsWith("g/") || req.startsWith("r/") || req.startsWith("y/")) {
#endif
                req = req.substring(2);
                return (req == "off" || req == "on" || req == "toggle");
            }
        }
    }
    return false;
}
bool Serve::Index::handle(ESP8266WebServer& webserver, HTTPMethod meth, String req) {
    this->web.requests++;
    this->web.txt.log("serve", req);
    this->web.txt.llg("#", String(this->web.requests, DEC));

    if (req == this->_index) {
        webserver.send(200, "text/html", this->web.index());
        return true;
    }
    if (req == this->_stats) {
        webserver.send(200, "application/json", this->web.stats(false));
        return true;
    }
    if (req.startsWith(this->_light)) {
        req = req.substring(this->_light.length());
        uint8_t num = (
            req.startsWith("g/") ? 0 :
                req.startsWith("r/") ? 1 :
#if VARIANT_LITE
                    0xff
#else
                    req.startsWith("y/") ? 2 : 0xff
#endif
        );
        req = req.substring(2);
        bool ok = false;
        if (req == "off") {
            ok = this->web.led.off(num);
        }
        if (req == "on") {
            ok = this->web.led.on(num);
        }
        if (req == "toggle") {
            ok = this->web.led.toggle(num);
        }

        Json json = Json(this->web.txt, false);
        json.add("name", this->web.led.get_name(num));
        json.add("action", ok ? req : "ERROR");
        webserver.send(ok ? 200 : 500, "application/json", json.show());
        return true;
    }
    return false;
}


String Serve::Json::show(void) {
    return this->txt.join("{", this->store, "}");
}
void Serve::Json::add(String key, String val, const bool raw) {
    String res = this->store.length() ? ", " : "";
    res = this->txt.join(res, "\"", key , "\": ");
    res = this->txt.join(res, raw ? val : this->txt.join("\"", val , "\""));
    this->store = this->txt.join(this->store, res);
    if (this->verbose) {
        this->txt.llg(key, val);
    }
}


uint8_t Serve::cmd_restart(String _) {
    ESP.restart();
    return 0;
}

uint8_t Serve::cmd_stats(String args) {
    this->txt.log("serve", "stats");
    if (args.length()) {
        this->txt.llg("VCC", String(ESP.getVcc(), DEC));
        this->txt.llg("MHz", String(ESP.getCpuFreqMHz(), DEC));
        this->txt.llg("chip id", String(ESP.getChipId(), HEX));
        this->txt.llg("flash id", String(ESP.getFlashChipId(), HEX));
        this->txt.llg("core version", ESP.getCoreVersion());
        this->txt.llg("boot version", String(ESP.getBootVersion(), DEC));
        this->txt.llg("free heap", String(ESP.getFreeHeap(), DEC));
        this->txt.llg("free sketch", String(ESP.getFreeSketchSpace(), DEC));
        this->txt.llg("sketch size", String(ESP.getSketchSize(), DEC));
        this->txt.llg("reset reason", ESP.getResetReason());
        this->txt.llg(":", this->txt.join("--", " ::"));
    }
    this->stats(true);
    return 0;
}
